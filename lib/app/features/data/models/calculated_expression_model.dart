import '../../domain/entities/calculated_expression.dart';

class CalculatedExpressionModel extends CalculatedExpression {
  const CalculatedExpressionModel(super.result);
}
