import 'package:dartz/dartz.dart';

import '../../../../calculator/calculator.dart';
import '../../../core/error/failures.dart';
import '../../domain/entities/calculated_expression.dart';
import '../../domain/repositories/calculator_repository.dart';
import '../../error/failures.dart';
import '../models/calculated_expression_model.dart';

class CalculatorRepositoryImpl implements CalculatorRepository {
  final Calculator calculator;

  CalculatorRepositoryImpl({required this.calculator});

  @override
  Future<Either<Failure, CalculatedExpression>> calculate(String text) async {
    try {
      final result = calculator.calculate(text);
      return Right(CalculatedExpressionModel(result));
    } catch (e) {
      return Left(CalculatorFailure());
    }
  }
}
