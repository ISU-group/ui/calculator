// not a good name
import 'package:equatable/equatable.dart';

class CalculatedExpression extends Equatable {
  final double result;

  const CalculatedExpression(this.result);

  @override
  List<Object?> get props => [result];
}
