import 'package:dartz/dartz.dart';

import '../../../core/error/failures.dart';
import '../../../core/use_cases/use_case.dart';
import '../entities/calculated_expression.dart';
import '../repositories/calculator_repository.dart';

class Calculate extends UseCase<CalculatedExpression, String> {
  final CalculatorRepository repository;

  Calculate(this.repository);

  @override
  Future<Either<Failure, CalculatedExpression>> call(String params) async {
    return await repository.calculate(params);
  }
}
