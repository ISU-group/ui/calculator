import 'package:dartz/dartz.dart';

import '../../../core/error/failures.dart';
import '../entities/calculated_expression.dart';

abstract class CalculatorRepository {
  Future<Either<Failure, CalculatedExpression>> calculate(String text);
}
