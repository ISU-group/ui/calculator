import 'package:flutter/material.dart';

class CalculatorButton extends StatelessWidget {
  final Color color;
  final Color textColor;
  final String text;
  final void Function(String)? onPressed;

  const CalculatorButton({
    super.key,
    required this.color,
    required this.textColor,
    required this.text,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: color,
      ),
      clipBehavior: Clip.antiAlias,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(side: BorderSide.none),
        onPressed: () => onPressed?.call(text),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: textColor,
              fontSize: 18,
            ),
          ),
        ),
      ),
    );
  }
}
