import 'package:flutter/material.dart';

import 'calculator_button.dart';

typedef ButtonCallback = void Function(String);

class ButtonMatrix extends StatelessWidget {
  final String buttonsText;
  final Map<String, ButtonCallback> buttonHandlers;
  final ButtonCallback defaultHandler;

  const ButtonMatrix({
    super.key,
    required this.buttonsText,
    required this.buttonHandlers,
    required this.defaultHandler,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final double itemHeight = (size.height - kToolbarHeight * 4) / 2;
    final double itemWidth = size.width / 2;

    return Center(
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          mainAxisExtent: 48,
          crossAxisCount: 4,
          crossAxisSpacing: 8,
          mainAxisSpacing: 8,
          childAspectRatio: (itemWidth / itemHeight),
        ),
        padding: const EdgeInsets.only(top: 16),
        itemCount: buttonsText.length,
        itemBuilder: (context, index) {
          final handler = buttonHandlers[buttonsText[index]] ?? defaultHandler;

          return CalculatorButton(
            color: Colors.white70,
            textColor: Colors.black87,
            text: buttonsText[index],
            onPressed: handler,
          );
        },
      ),
    );
  }
}
