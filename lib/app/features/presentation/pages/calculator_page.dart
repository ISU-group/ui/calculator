import 'package:calc/app/features/presentation/utils/calculation_result.dart';
import 'package:calc/app/features/presentation/utils/calculator_input_controller.dart';
import 'package:flutter/material.dart';

import '../../domain/use_cases/calculate.dart';
import '../widgets/button_matrix.dart';

import 'handlers/calculator_handler.dart';

class CalculatorPage extends StatefulWidget {
  final Calculate calculate;
  const CalculatorPage({super.key, required this.calculate});

  @override
  State<CalculatorPage> createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  final buttonsText = 'C*/⌫789-456+123%^0.=';

  final calculationResult = CalculationResult();
  final inputController = CalculatorInputController(
    controller: TextEditingController(),
    focusNode: FocusNode(),
  );

  late final CalculatorHandler _handler = CalculatorHandler(
    inputController: inputController,
    calculationResult: calculationResult,
    calculate: widget.calculate,
  );

  late final Map<String, ButtonCallback> buttonHandlers = {
    'C': _onClearButtonPressed,
    '⌫': _onRemoveButtonPressed,
    '=': _onGetResultButtonPressed,
  };

  void _onClearButtonPressed(String text) {
    _handler.onClearButtonPressed(text);
    setState(() {});
  }

  void _onRemoveButtonPressed(String text) {
    _handler.onRemoveButtonPressed(text);
  }

  void _onGetResultButtonPressed(String text) {
    _handler.onGetResultButtonPressed(text);
    setState(() {});
  }

  void _onButtonPressedDefault(String text) {
    _handler.onButtonPressedDefault(text);
    setState(() {});
  }

  @override
  void dispose() {
    inputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.lightGreen.shade300,
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              children: [
                _inputFieldWithOutput(),
                Expanded(
                  child: SizedBox(
                    width: 300,
                    height: 420,
                    child: ButtonMatrix(
                      buttonsText: buttonsText,
                      buttonHandlers: buttonHandlers,
                      defaultHandler: _onButtonPressedDefault,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _inputFieldWithOutput() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.lightGreen[50],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          TextField(
            showCursor: false,
            readOnly: true,
            controller: inputController.controller,
            focusNode: inputController.focusNode,
            autofocus: true,
            style: const TextStyle(fontSize: 24),
            decoration: const InputDecoration(border: InputBorder.none),
            textAlign: TextAlign.right,
            textInputAction: TextInputAction.none,
            onSubmitted: _onGetResultButtonPressed,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: Text(
              calculationResult.toString(),
              style: const TextStyle(
                fontSize: 32,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
