import 'package:calc/app/features/presentation/utils/calculation_result.dart';
import 'package:calc/app/features/presentation/utils/calculator_input_controller.dart';

import '../../../domain/use_cases/calculate.dart';

class CalculatorHandler {
  final CalculatorInputController inputController;
  final CalculationResult calculationResult;
  final Calculate calculate;

  CalculatorHandler({
    required this.inputController,
    required this.calculationResult,
    required this.calculate,
  });

  void onClearButtonPressed(String text) {
    inputController.controller.clear();
    calculationResult.clear();
    _requestFocus();
  }

  void _requestFocus() {
    inputController.focusNode.requestFocus();
  }

  void onRemoveButtonPressed(String _) {
    if (inputController.controller.text.isNotEmpty) {
      if (!_tryRemoveExponential()) {
        _removeLastSymbol();
      }
    }
    _requestFocus();
  }

  bool _tryRemoveExponential() {
    final exp = RegExp(r'(.*)e[\+\-]?\d$');

    final match = exp.firstMatch(inputController.controller.text);
    if (match != null) {
      inputController.controller.text = match[1]!;
      return true;
    }
    return false;
  }

  _removeLastSymbol() {
    final text = inputController.controller.text;
    inputController.controller.text = text.substring(0, text.length - 1);
  }

  void onGetResultButtonPressed(String text) {
    if (inputController.controller.text.isNotEmpty) {
      _calculate();
      _requestFocus();
    }
  }

  void _calculate() async {
    final text = inputController.controller.text;

    (await calculate(text)).fold(
      (failure) => calculationResult.setMessage('Error'),
      (calculatedExpression) =>
          calculationResult.update(calculatedExpression.result),
    );
  }

  void onButtonPressedDefault(String text) {
    if (calculationResult.hasResult()) {
      if (calculationResult.isValid()) {
        if (_isMathOperator(text)) {
          inputController.controller.text = calculationResult.toString() + text;
        } else {
          inputController.controller.text = text;
        }
        calculationResult.setMessage('');
      }
    } else {
      inputController.controller.text += text;
    }
    _requestFocus();
  }

  bool _isMathOperator(String text) {
    const operators = ['+', '-', '*', '/', '^', '%'];
    return operators.contains(text);
  }
}
