import 'package:flutter/widgets.dart';

class CalculatorInputController {
  final TextEditingController controller;
  final FocusNode focusNode;

  CalculatorInputController({
    required this.controller,
    required this.focusNode,
  });

  void dispose() {
    controller.dispose();
    focusNode.dispose();
  }
}
