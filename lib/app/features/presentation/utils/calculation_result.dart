class CalculationResult {
  double? _result;
  String _message = '';

  void clear() {
    update(null);
  }

  void update(double? result, [String? message]) {
    _result = result;
    _message = message ?? '';
  }

  void setMessage(String? message) {
    update(null, message ?? 'Error');
  }

  bool hasResult() => _result != null;

  bool isValid() =>
      hasResult() ? !(_result!.isInfinite || _result!.isNaN) : false;

  @override
  String toString() {
    if (_result != null) {
      final truncated = _result!.truncateToDouble();

      if (truncated.isInfinite || truncated.isNaN) {
        _message = truncated.toString();
      } else {
        _message = truncated == _result!
            ? truncated.toInt().toString()
            : _result!.toStringAsPrecision(4);
      }
    }

    return _message;
  }
}
