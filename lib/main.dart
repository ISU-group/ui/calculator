import 'dart:io';

import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';
import 'package:flutter/services.dart';

import 'app/features/data/repositories/calculator_repository_impl.dart';
import 'app/features/domain/use_cases/calculate.dart';
import 'app/features/presentation/pages/calculator_page.dart';
import 'calculator/calculator.dart';
import 'calculator/parser/parser.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  _disableLandscapeMode();

  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    setWindowTitle("Dummy Calculator");
    setWindowMinSize(const Size(300, 520));
    setWindowMaxSize(const Size(420, 640));
  }

  final calculate = Calculate(
    CalculatorRepositoryImpl(
      calculator: Calculator(parser: Parser()),
    ),
  );

  runApp(MyApp(calculate: calculate));
}

void _disableLandscapeMode() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
}

class MyApp extends StatelessWidget {
  final Calculate calculate;

  const MyApp({super.key, required this.calculate});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: CalculatorPage(
        calculate: calculate,
      ),
    );
  }
}
