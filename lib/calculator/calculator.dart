import 'dart:math';

import 'parser/node.dart';
import 'parser/parser.dart';
import 'parser/token.dart';

class Calculator {
  final Parser parser;

  Calculator({required this.parser});

  double calculate(String text) {
    try {
      final tree = parser.parse(text);
      return _visit(tree);
    } on ParserException {
      throw CalculatorException();
    }
  }

  double _visit(Node tree) {
    if (tree is Number) {
      return _visitNumber(tree);
    } else if (tree is BinOp) {
      return _visitBinOp(tree);
    } else if (tree is UnaryOp) {
      return _visitUnaryOp(tree);
    } else {
      throw CalculatorException();
    }
  }

  double _visitNumber(Number tree) {
    return double.parse(tree.token.value);
  }

  double _visitBinOp(BinOp tree) {
    final op = tree.op.type;
    final lhs = _visit(tree.lhs);
    final rhs = _visit(tree.rhs);

    switch (op) {
      case TokenType.plus:
        return lhs + rhs;
      case TokenType.minus:
        return lhs - rhs;
      case TokenType.mul:
        return lhs * rhs;
      case TokenType.div:
        return lhs / rhs;
      case TokenType.pow:
        return pow(lhs, rhs) as double;
      case TokenType.percent:
        return lhs * rhs * 0.01;
      default:
        throw CalculatorException();
    }
  }

  double _visitUnaryOp(UnaryOp tree) {
    final op = tree.op.type;

    switch (op) {
      case TokenType.minus:
        return -_visit(tree.node);
      case TokenType.plus:
        return _visit(tree.node);
      default:
        throw CalculatorException();
    }
  }
}

class CalculatorException implements Exception {}
