import 'package:equatable/equatable.dart';

import 'token.dart';

abstract class Node extends Equatable {}

class Number extends Node {
  final Token token;

  Number(this.token);

  @override
  String toString() => 'Number(${token.value})';

  @override
  List<Object?> get props => [token];
}

class BinOp extends Node {
  final Node lhs, rhs;
  final Token op;

  BinOp({
    required this.lhs,
    required this.op,
    required this.rhs,
  });

  @override
  String toString() => '<${op.value}>($lhs, $rhs)';

  @override
  List<Object?> get props => [lhs, op, rhs];
}

class UnaryOp extends Node {
  final Node node;
  final Token op;

  UnaryOp({
    required this.node,
    required this.op,
  });

  @override
  String toString() => '<${op.value}>($node)';

  @override
  List<Object?> get props => [node, op];
}

class EosNode extends Node {
  @override
  List<Object?> get props => [];
}
