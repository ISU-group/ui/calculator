import 'package:dartz/dartz.dart';

import 'lexer.dart';
import 'node.dart';
import 'token.dart';

class Parser {
  final Lexer _lexer = Lexer();
  Option<Token> _currentToken = const None();

  Node parse(String text) {
    _lexer.init(text);
    _currentToken = _lexer.next().toOption();
    return _buildTree();
  }

  Node _expr() {
    const ops = [TokenType.plus, TokenType.minus];
    Node result = _term();

    while (_isExpectedOp(_currentToken, ops)) {
      _currentToken.fold(
        () => null,
        (token) {
          _updateCurrentTokenIfTypeEq(token.type);
          result = BinOp(lhs: result, op: token, rhs: _term());
        },
      );
    }

    return result;
  }

  Node _term() {
    Node result = _powTerm();
    const ops = [TokenType.mul, TokenType.div, TokenType.percent];

    while (_isExpectedOp(_currentToken, ops)) {
      _currentToken.fold(
        () => null,
        (token) {
          _updateCurrentTokenIfTypeEq(token.type);
          result = BinOp(lhs: result, op: token, rhs: _powTerm());
        },
      );
    }

    return result;
  }

  Node _powTerm() {
    Node result = _factor();
    const ops = [TokenType.pow];

    while (_isExpectedOp(_currentToken, ops)) {
      _currentToken.fold(
        () => null,
        (token) {
          _updateCurrentTokenIfTypeEq(token.type);
          result = BinOp(lhs: result, op: token, rhs: _powTerm());
        },
      );
    }

    return result;
  }

  bool _isExpectedOp(Option<Token> token, List<TokenType> ops) {
    return token.fold(() => false, (token) => ops.contains(token.type));
  }

  void _updateCurrentTokenIfTypeEq(TokenType type) {
    _currentToken.fold(
      () => throw ParserException(message: 'No token supplied'),
      (token) {
        if (token.type == type) {
          _currentToken = _lexer.next().toOption();
        } else {
          throw ParserException(
            message:
                'Invalid token type - expected($type), actual(${token.type}',
          );
        }
      },
    );
  }

  Node _factor() {
    return _currentToken.fold(
      () => throw ParserException(message: 'No token supplied'),
      (token) {
        switch (token.type) {
          case TokenType.number:
            _updateCurrentTokenIfTypeEq(TokenType.number);
            return Number(token);
          case TokenType.lparen:
            _updateCurrentTokenIfTypeEq(TokenType.lparen);
            final result = _expr();
            _updateCurrentTokenIfTypeEq(TokenType.rparen);
            return result;
          case TokenType.minus:
          case TokenType.plus:
            _updateCurrentTokenIfTypeEq(token.type);
            return UnaryOp(node: _term(), op: token);
          case TokenType.eos:
            return EosNode();
          default:
            throw ParserException(message: 'Invalid token: $token');
        }
      },
    );
  }

  Node _buildTree() {
    final tree = _expr();

    _currentToken.fold(
      () => throw ParserException(message: 'Undefined token'),
      (token) {
        if (token.type != TokenType.eos) {
          throw ParserException(message: 'Invalid expression');
        }
      },
    );

    return tree;
  }
}

class ParserException implements Exception {
  final String? message;

  ParserException({this.message});
}
