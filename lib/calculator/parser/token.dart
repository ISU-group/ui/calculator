import 'package:equatable/equatable.dart';

class Token extends Equatable {
  final TokenType type;
  final String value;

  const Token(this.type, {required this.value});

  @override
  String toString() {
    return 'Token($type, $value)';
  }

  @override
  List<Object?> get props => [type, value];
}

enum TokenType {
  number,
  plus,
  minus,
  mul,
  div,
  pow,
  percent,
  lparen,
  rparen,
  dot,
  eos,
}
