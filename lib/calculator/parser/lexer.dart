import 'package:dartz/dartz.dart';

import '../error.dart';
import 'token.dart';

const _eosToken = Token(TokenType.eos, value: '');

class Lexer {
  int _position = -1;
  Option<String> _currentChar = const None();
  String _text = '';

  static const _operatorToTokenType = {
    '+': TokenType.plus,
    '-': TokenType.minus,
    '*': TokenType.mul,
    '/': TokenType.div,
    '^': TokenType.pow,
    '%': TokenType.percent,
    '(': TokenType.lparen,
    ')': TokenType.rparen,
  };

  void init(String text) {
    _text = text;
    _position = -1;
    _forward();
  }

  void _forward() {
    _position += 1;
    _currentChar = _hasReadAll() ? const None() : Some(_text[_position]);
  }

  bool _hasReadAll() => _position >= _text.length;

  Either<Error, Token> next() {
    while (!_currentChar.isNone()) {
      if (_isCurrentCharWhitespace()) {
        _skipWhitespace();
        continue;
      }

      return _getNextToken();
    }

    return const Right(_eosToken);
  }

  bool _isCurrentCharWhitespace() => _currentChar == const Some(' ');

  void _skipWhitespace() {
    while (_isCurrentCharWhitespace()) {
      _forward();
    }
  }

  Either<Error, Token> _getNextToken() {
    return _currentChar.fold(
      () => const Right(_eosToken),
      (char) {
        if (_isDigit(char) || _isDot(char)) {
          final number = _number();
          return Right(Token(TokenType.number, value: number));
        }

        return _getOperator(char).fold(
          () => Left(LexerError(message: 'Undefined token: $char')),
          (token) => Right(token),
        );
      },
    );
  }

  bool _isDigit(String source) => double.tryParse(source) != null;
  bool _isDot(String source) => '.' == source;

  String _number() {
    final integer = _getInteger();
    if (_isCurrentCharDot()) {
      final decimal = _getDecimal();
      return '$integer.$decimal';
    }

    return integer;
  }

  String _getInteger() {
    final digits = [];
    while (_isCurrentCharDigit()) {
      digits.add(_currentChar.getOrElse(() => ''));
      _forward();
    }

    return digits.join('');
  }

  bool _isCurrentCharDigit() =>
      _currentChar.fold(() => false, (char) => _isDigit(char));

  bool _isCurrentCharDot() => _currentChar == const Some('.');

  String _getDecimal() {
    _forward();
    return _getInteger();
  }

  Option<Token> _getOperator(String operator) {
    final tokenType = _operatorToTokenType[operator];
    if (tokenType == null) {
      return const None();
    }

    _forward();
    return Some(Token(tokenType, value: operator));
  }
}

class LexerException implements Exception {
  final String? message;

  LexerException({this.message});
}
