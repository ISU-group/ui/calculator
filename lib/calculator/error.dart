import 'package:equatable/equatable.dart';

abstract class Error extends Equatable {
  final String? message;

  const Error({this.message});
}

class LexerError extends Error {
  const LexerError({super.message});

  @override
  List<Object?> get props => [];
}
