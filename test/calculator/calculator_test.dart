import 'package:calc/calculator/calculator.dart';
import 'package:calc/calculator/parser/parser.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../utils/pair.dart';

class MockParser extends Mock implements Parser {}

void main() {
  group(
    'successful calculation',
    () {
      final parser = Parser();
      final calculator = Calculator(parser: parser);

      final expressionsAndResults = <Pair<String, double>>[
        Pair(".5 + 5", 5.5),
        Pair("1 * (2.54 + 3)", 5.54),
        Pair("1.5 * (2 + 3)", 7.5),
        Pair("1 * 2 + 3", 5),
        Pair("-1", -1),
        Pair("+1", 1),
        Pair("-(1 + 1)^3^2", -512),
        Pair("2*3^3", 54),
        Pair("-1.5^2", -2.25),
        Pair("10 + 2^3^2", 522),
        Pair("2 % 10", 0.2),
      ];

      test(
        'should calculate all expressions correctly',
        () {
          for (var pair in expressionsAndResults) {
            final result = calculator.calculate(pair.first);

            expect(result, closeTo(pair.second, 0.00001));
          }
        },
      );
    },
  );

  group(
    'unsuccessful calculation',
    () {
      final mockParser = MockParser();
      final calculator = Calculator(parser: mockParser);

      setUp(() {
        when(() => mockParser.parse(any())).thenThrow(ParserException());
      });

      test(
        'should throw [CalculatorException] when input is invalid',
        () async {
          // arrange
          const invalidText = 'qwerty';
          // assert
          expect(
            () => calculator.calculate(invalidText),
            throwsA(isA<CalculatorException>()),
          );
          verify(() => mockParser.parse(any()));
        },
      );

      test(
        'should throw [CalculatorException] when input is partially invalid',
        () async {
          // arrange
          const invalidText = '1 + 0.1sdf';
          // assert
          expect(
            () => calculator.calculate(invalidText),
            throwsA(isA<CalculatorException>()),
          );
          verify(() => mockParser.parse(any()));
        },
      );
    },
  );
}
