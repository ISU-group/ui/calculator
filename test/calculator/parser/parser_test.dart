import 'package:calc/calculator/parser/node.dart';
import 'package:calc/calculator/parser/parser.dart';
import 'package:calc/calculator/parser/token.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../utils/pair.dart';

void main() {
  final parser = Parser();
  final exprsAndNodes = [
    Pair(
      "2 + 3",
      BinOp(
        lhs: Number(const Token(TokenType.number, value: '2')),
        op: const Token(TokenType.plus, value: '+'),
        rhs: Number(const Token(TokenType.number, value: '3')),
      ),
    ),
    Pair(
      "2 % 10",
      BinOp(
        lhs: Number(const Token(TokenType.number, value: '2')),
        op: const Token(TokenType.percent, value: '%'),
        rhs: Number(const Token(TokenType.number, value: '10')),
      ),
    ),
    Pair(
      "-(1 + 1)^3^2",
      UnaryOp(
        op: const Token(TokenType.minus, value: '-'),
        node: BinOp(
          lhs: BinOp(
            lhs: Number(const Token(TokenType.number, value: '1')),
            op: const Token(TokenType.plus, value: '+'),
            rhs: Number(const Token(TokenType.number, value: '1')),
          ),
          op: const Token(TokenType.pow, value: '^'),
          rhs: BinOp(
            lhs: Number(const Token(TokenType.number, value: '3')),
            op: const Token(TokenType.pow, value: '^'),
            rhs: Number(const Token(TokenType.number, value: '2')),
          ),
        ),
      ),
    ),
    Pair(
      "-(1 + 1)^3*2",
      UnaryOp(
        op: const Token(TokenType.minus, value: '-'),
        node: BinOp(
          lhs: BinOp(
            lhs: BinOp(
              lhs: Number(const Token(TokenType.number, value: '1')),
              op: const Token(TokenType.plus, value: '+'),
              rhs: Number(const Token(TokenType.number, value: '1')),
            ),
            op: const Token(TokenType.pow, value: '^'),
            rhs: Number(const Token(TokenType.number, value: '3')),
          ),
          op: const Token(TokenType.mul, value: '*'),
          rhs: Number(
            const Token(TokenType.number, value: '2'),
          ),
        ),
      ),
    ),
    Pair(
      "-1",
      UnaryOp(
          node: Number(const Token(TokenType.number, value: '1')),
          op: const Token(TokenType.minus, value: '-')),
    ),
    Pair(
      "10 + 2^3^2",
      BinOp(
        lhs: Number(const Token(TokenType.number, value: '10')),
        op: const Token(TokenType.plus, value: '+'),
        rhs: BinOp(
          lhs: Number(const Token(TokenType.number, value: '2')),
          op: const Token(TokenType.pow, value: '^'),
          rhs: BinOp(
            lhs: Number(const Token(TokenType.number, value: '3')),
            op: const Token(TokenType.pow, value: '^'),
            rhs: Number(const Token(TokenType.number, value: '2')),
          ),
        ),
      ),
    ),
    Pair("", EosNode()),
  ];

  group('successful parsing', () {
    test(
      'should return [Node] tree when input is valid',
      () async {
        for (final pair in exprsAndNodes) {
          // act
          final result = parser.parse(pair.first);
          // assert
          expect(result, equals(pair.second));
        }
      },
    );
  });

  group(
    'unsuccessful parsing',
    () {
      test('should throw [ParserException] when input is invalid', () async {
        // assert
        expect(() => parser.parse("qwerty"), throwsA(isA<ParserException>()));
      });

      test('should throw [ParserException] when input is strange and invalid',
          () async {
        // assert
        expect(
          () => parser.parse(".5 + .5.5"),
          throwsA(isA<ParserException>()),
        );
      });

      test(
        'should throw [ParserException] when input is partially invalid',
        () async {
          // assert
          expect(
            () => parser.parse("1 + 0.1sdf"),
            throwsA(isA<ParserException>()),
          );
        },
      );
    },
  );
}
