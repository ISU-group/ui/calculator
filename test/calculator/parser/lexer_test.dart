import 'package:calc/calculator/error.dart';
import 'package:calc/calculator/parser/lexer.dart';
import 'package:calc/calculator/parser/token.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../utils/pair.dart';

void main() {
  final lexer = Lexer();
  final inputTextAndTokens = [
    Pair('.5', const [Token(TokenType.number, value: '.5')]),
    Pair("4 +3", const [
      Token(TokenType.number, value: '4'),
      Token(TokenType.plus, value: '+'),
      Token(TokenType.number, value: '3'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("4 -3", const [
      Token(TokenType.number, value: '4'),
      Token(TokenType.minus, value: '-'),
      Token(TokenType.number, value: '3'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("4 *3", const [
      Token(TokenType.number, value: '4'),
      Token(TokenType.mul, value: '*'),
      Token(TokenType.number, value: '3'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("4 /2", const [
      Token(TokenType.number, value: '4'),
      Token(TokenType.div, value: '/'),
      Token(TokenType.number, value: '2'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("2^3", const [
      Token(TokenType.number, value: '2'),
      Token(TokenType.pow, value: '^'),
      Token(TokenType.number, value: '3'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("2 * (2 + 3)", const [
      Token(TokenType.number, value: '2'),
      Token(TokenType.mul, value: '*'),
      Token(TokenType.lparen, value: '('),
      Token(TokenType.number, value: '2'),
      Token(TokenType.plus, value: '+'),
      Token(TokenType.number, value: '3'),
      Token(TokenType.rparen, value: ')'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("-1", const [
      Token(TokenType.minus, value: '-'),
      Token(TokenType.number, value: '1'),
      Token(TokenType.eos, value: ''),
    ]),
    Pair("2%10", const [
      Token(TokenType.number, value: '2'),
      Token(TokenType.percent, value: '%'),
      Token(TokenType.number, value: '10'),
      Token(TokenType.eos, value: ''),
    ])
  ];

  test(
    'should tokenize a given text',
    () async {
      // act
      for (var pair in inputTextAndTokens) {
        lexer.init(pair.first);

        for (int i = 0; i < pair.second.length; i++) {
          final errorOrToken = lexer.next();
          final expectedToken = pair.second[i];
          // assert
          expect(errorOrToken, equals(Right(expectedToken)));
        }
      }
    },
  );

  test(
    'should return [LexerError] when invalid text is given',
    () async {
      // arrange
      const invalidText = 'qwerty';
      lexer.init(invalidText);
      // act
      final result = lexer.next();
      // assert
      expect(result, const Left(LexerError()));
    },
  );
}
