import 'package:calc/calculator/parser/node.dart';
import 'package:calc/calculator/parser/token.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../utils/pair.dart';

void main() {
  const tokenNumber = Token(TokenType.number, value: '0');
  final number = Number(tokenNumber);
  const op = Token(TokenType.minus, value: '-');
  final nodes = [
    Pair(number, "Number(0)"),
    Pair(BinOp(lhs: number, op: op, rhs: number), "<->($number, $number)"),
    Pair(UnaryOp(node: number, op: op), "<->($number)"),
  ];

  test(
    'should return string representation of [Node] inherited classes',
    () async {
      // act
      for (final pair in nodes) {
        // assert
        expect(pair.first.toString(), equals(pair.second));
      }
    },
  );
}
