import 'package:calc/calculator/parser/token.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const token = Token(TokenType.number, value: '2');
  const expectedString = 'Token(TokenType.number, 2)';

  test('should return Token string representation', () {
    // act
    final result = token.toString();
    // assert
    expect(result, equals(expectedString));
  });
}
