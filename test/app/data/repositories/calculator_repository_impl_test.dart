import 'package:calc/app/features/data/models/calculated_expression_model.dart';
import 'package:calc/app/features/data/repositories/calculator_repository_impl.dart';
import 'package:calc/app/features/error/failures.dart';
import 'package:calc/calculator/calculator.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockCalculator extends Mock implements Calculator {}

void main() {
  final calculator = MockCalculator();
  final repository = CalculatorRepositoryImpl(calculator: calculator);

  test(
    'should return the correct result of the calculation',
    () async {
      // arrange
      const text = '2 + 2';
      const expected = CalculatedExpressionModel(4);

      when(() => calculator.calculate(text)).thenReturn(4);
      // act
      final result = await repository.calculate(text);
      // assert
      verify(() => calculator.calculate(any()));
      expect(result, equals(const Right(expected)));
    },
  );

  test(
    'should return [CalculatorFailure] when calculation is unsuccessful',
    () async {
      // arrange
      const invalidText = 'qwerty';
      when(() => calculator.calculate(any())).thenThrow(CalculatorException());
      // act
      final result = await repository.calculate(invalidText);
      // assert
      expect(result, equals(Left(CalculatorFailure())));
    },
  );
}
