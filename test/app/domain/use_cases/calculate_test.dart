import 'package:calc/app/features/domain/entities/calculated_expression.dart';
import 'package:calc/app/features/domain/repositories/calculator_repository.dart';
import 'package:calc/app/features/domain/use_cases/calculate.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockCalculatorRepository extends Mock implements CalculatorRepository {}

void main() {
  final repository = MockCalculatorRepository();
  final useCase = Calculate(repository);

  final expectedResult = CalculatedExpression(0);
  const text = '0';

  test(
    'should calculate a mathematical expression with the repository call',
    () async {
      // arrange
      when(() => repository.calculate(text))
          .thenAnswer((invocation) async => Right(expectedResult));
      // act
      final result = await useCase(text);
      // assert
      expect(result, equals(Right(expectedResult)));

      // Verify that the repository has been called
      verify(() => repository.calculate(text));
      // Verify that only the above method has been called and nothing more
      verifyNoMoreInteractions(repository);
    },
  );
}
